$(document).ready(function () {
   //Mise en place du boutton "details"
   $(".details").on("click", function () {
      $(".voile").slideDown(800); //Voile qui arrive;
      $(".voile .vignette").slideDown(1000);
      $(".titreVign").html("<h2>" + $(this).attr("data-dest") + "</h2>"); //Titre de la vignette
      $(".imgVign").children("img").attr("src", "images/" + $(this).attr("data-dest") + ".jpg"); //Affiche l'image
      $.get('http://localhost/Jquerry/tp9/fiches/' + $(this).attr("data-dest") + ".txt", function (data) {
         $(".textVign").html(data);
      })
      //bouton de la vignette pour la fermer
      $(".Vign").on("click", function () {

         $(".voile .vignette").slideUp(800);
         $(".voile").slideUp(1600)
      });
   });
}) //Fin document.ready